# **How to use Git and BitBucket with SourceTree** #

## **Introduction** ##
This tutorial will show you how to use **Git** and BitBucket efficiently to sync data and keep track on how files have been changed over time. To do this, you'll be using a software called SourceTree.

This tutorial will cover: 

* how to create a **repository** and **clone** it to your computer

* how to sync data using the commands **commit**, **pull** and **push** 

* how to handle **merge conflicts**

* how to utilize **issues** and **milestones** to create a solid project plan

* how to use **branches** and **pull requests** so that separate tasks can be done in parallel with each other

## **How to create and clone a repository** ##
### 1. Creating the repository ###
Creating a **repository** is easily done at [bitbucket.org](https://bitbucket.org/). If you're not already logged in to your account, log in and you'll get to the dashboard page. If you don't have any **repositories** in your dashboard already, you'll see a button labeled **Create a repository** in the middle of the page. Otherwise, you'll have to click on the drop down menu, labeled **Repositories**, in the header, and then click on **Create repository**. 

Once you've done that, BitBucket will ask for some information for your new **repo**. You'll need to enter a **name**, whether the **repository** should be **public** or **private**, and whether to use **Git** or **Mercurial**. Make sure you choose **Git** for this tutorial. There are also some advanced settings. Most of it won't be covered in this tutorial, but make sure you check the box marked as **Issue tracking**. More on that later. 

Great, you've now created your **repository**! Now it's time to **clone** it to your computer so you can start working with it!

### 2. Installing SourceTree ###
SourceTree is easy to install, just go to [this link](https://www.sourcetreeapp.com/) and download the software. It will ask for some credentials and then you'll be looking at a screen like this: 
![gitTut1.png](https://bitbucket.org/repo/pkbBLj/images/2550190061-gitTut1.png)
Now you're ready to **clone** your previously created **repo**. This can be done in two ways.

### 3a. Cloning your repository through BitBucket ###
The first way of **cloning** your **repo** is by doing it from the **repo** itself. If you open up your **repository** created in step 1 online on BitBucket, you'll find yourself at the overview page of the **repo**. You should see a button labeled **Clone in SourceTree**. Press this button, and your browser will try to open SourceTree and enter some credentials into the software for you. It will look somewhat like this: 
![gitTut2.png](https://bitbucket.org/repo/pkbBLj/images/3497103271-gitTut2.png)
Change the path to your preferred folder, and then press the **clone** button. Now you have a **cloned** copy of your **repo**.

### 3b. Cloning your repository through SourceTree ###
The second way of cloning your **repo** is by doing it in SourceTree. From the page looking like the one in the first picture, press the button labeled **+ New Repository**. After that, click on **Clone from URL**. A window will pop up looking like this: 
![gitTut3.png](https://bitbucket.org/repo/pkbBLj/images/687276855-gitTut3.png)
What you want to type in is the **Source URL**. This can be found in your **repository** page on BitBucket. Go to your **repo**, and click on the button labeled **Clone** in the margin to the left. In the window that shows, make sure you've selected HTTPS in the drop down menu. The text field should now be filled with the words **git clone** as well as a link. Copy *only* the link and paste it in the **Source URL** field in SourceTree. The other fields should be filled in themselves when you click on one of them, but otherwise, choose the path you want your local **repository** to be stored in, and the name of the **repo**.

After you've done this, press the **clone** button and you're done!

## **How to sync data** ##
### 1. Create some data and add it to your repository ###
Okay, so now that you've created your **repository**, you want to put stuff in it, right? If you look at your **repository** in SourceTree right now it looks empty:
![gitTut4.png](https://bitbucket.org/repo/pkbBLj/images/2895141327-gitTut4.png)
For the sake of this tutorial, we're going to populate the **repo** with a text file. Open your preferred text editor and create a new file. This file will contain information about your favorites, so a fitting name for the file would be exactly that, favorites. Type your favorite song in the file and save it in your local **repository**. Now open SourceTree and, if it's not already open, open the **repo** you saved the file in. SourceTree should have recognized the added file, and you'll have something that looks like this: 
![gitTut5.png](https://bitbucket.org/repo/pkbBLj/images/534984621-gitTut5.png) 

### 2. Commit your data to the repository ###
Right now, you've only added the file to your local **repository**, but the **repository** online doesn't know about the file. What you have to do now is stage the file for a **commit**, so that it later on can be **pushed** online. If you look in the window labeled **Unstaged files** you will see your previously created text file. If you click on it, you can see what it contains in the window to the right. Now, check the box to the left of the file. You'll see that the file moves from the **Unstaged files** to the **Staged files**. Another thing you will notice is that the added line containing your favorite song is now marked in **green**. This colour shows that something has been added to the file. The opposite, that something has been removed from the file, is marked with **red**. 

Your file is now ready to be committed. In the top left you will see a button labeled **Commit**. Press it. If it's your first **commit**, SourceTree might ask for some information, so fill that in and then press OK. You will see a window with a writable text field pop up in the bottom of the software. This is where you'll write your **commit message**.

A **commit message** is *very* important when working with version control like **Git**. It describes what kind of changes has been made in the **repository**. Because of this, it's important to write clear messages, so that collaborators understand what has been changed and so that someone will know what they're looking at if they go back to look at older code. 

There are a few rules of thumb regarding **commits** and **commit messages**:

* A **commit** should contain only one, small thing. Let's say you're creating a game and you're working on enemies. It would be bad practice to create all enemies and **commit** them all at once in the same **commit**. Instead, it's better to create one enemy at a time and have separate **commits** for each of them. Sometimes it's good to divide it even further. However, don't make **commits** too small, 2 lines of code for each **commit** will only clutter your **repo**.

* **Commit messages** should only describe what has been changed in the files, and nothing else. They should explain the changes, but with as few words as possible. Try to write **commit messages** under 70 characters. If that's hard to do, you should think about splitting the **commit** into several **commits**.

* Keep to one tense when writing **commits** in your **repository**. Most people use either present tense or past tense when writing their **commits**, but using both in the same **repo** will only make the commits less readable. 

* It is possible to write more detailed explanations about a **commit** inside the **commit message**, but do that on a new line. The first line should only cover the absolute fundamentals.

For this **commit**, write something along the lines of: "Added file containing my favorites" (or "Add file containing my favorites" if you prefer present tense). Once you've done that make sure the button labeled **Push changes immediately to origin/master** isn't checked, and press the **commit** button.

The window will now revert back to its original form, looking empty. This is because the **repository** now knows about all the files in it, there's nothing new that needs to be **committed**. If you press the **History** label in the left margin, you can see a history of your **commits**. It's now time to **push** the **commit** online!

### 3. Push your data to the online repository ###
By **pushing**, you send all **commits** since your last **push** to the online version of your **repository**. In the top of the window, you'll see a button labeled **Push**. Click on it, and a window should appear. This window asks you where you want to **push** the commits, and show different **branches** that you can **push** to. We'll talk more about that later, but right now, just check the box labeled **master** and press the OK button. 

If you now go to your online **repo** on BitBucket, you'll see some differences. First of all, the overview has changed, there's no longer a **Clone** button in the middle of the window. If you look to the right you'll see a list labeled **Recent activity**. You'll see your recently **pushed** **commit** here. If you press the number and letter combination next to the **commit message**, you'll be taken to a window where you can see all the changes that happened with your **commit**. Right now, you should only see a line containing your favorite song, marked in **green**. 

Also, if you now go to the **Source** tab that can be found in the left margin, you can see that the favorites file now exists in your online **repository**. 

### 4. Make some changes online ###
A good thing with BitBucket is that you can edit files online. Navigate to the **Source** tab and click on your favorites file. This will show you what the file contains (BitBucket automatically shows the information contained in every file that isn't a binary one). You'll also notice an **edit** button next to the file info. If you press this, you'll be able to make changes online. Do this, and write your favorite TV-show on a new line. 

You'll now see that there's a **commit** button here as well. Press that button and BitBucket will ask you for a **commit message**. Like before, this message should describe what you've done with the file. Write something like "Added favorite TV-show to favorites" and press **commit**. This will automatically be **pushed** to the online **repo**.

### 5. Pull the changes made online to your local repository ###
If you look at your favorites file in your local **repository** on your computer, you will see that it does not contain your favorite TV-show. That is because the change you made online only affected the online **repo**, and not your local copy. You now have to bring your local **repository** up to speed with the online one by **pulling** the information from it. 

To do this, open up SourceTree and press the **Pull** button at the top. Don't worry about the different options given, just press the OK button. SourceTree should now **pull** down the edited file to your local **repository**, and if you look in the favorites file now, you'll see your favorite TV-show. You can also check the **History** tab in the left margin of SourceTree, and you'll see both commits, the one made locally and the one made online, and you can see how the file has changed from one point to the next. This is the beauty of version control, everything you've done, *EVER*, can be easily accessible!

### 6. How to use these commands ###
Great work, you've now learnt the most important part of **Git**; **committing**, **pushing** and **pulling**! But it's also important to use them correctly. Before you start working, make sure you **pull** from the online **repo**, so that updated files are updated locally as well. After that, you can do some **commits**, and then it's good to do another **pull** to make sure that changes a collaborator has made gets copied to your local **repo**. After all of this, you can **push** your changes to the online **repo**. This way is the way that is least prone to errors, something that we'll be talking about in the next section. 

## **How to handle merge conflicts and other errors** ##
### 1. What happens if you don't follow the pull, commit, push order? ###
We'll now see what happens when you don't follow the order specified in the previous section. Imagine that you're collaborating on this project with your friend Tony. Tony goes onto BitBucket and checks the **repo**, and sees that you've added a favorites file to the **repo**. He looks inside it, and realizes you haven't written anything about favorite food. Tony opens the online editor and writes "Pizza" on a new line, and then **commits** the change with the **commit message** "Added favorite food to favorites". 

Pretend that you're Tony, and do the changes mentioned above to the **repo**. When you're done you can go back to being yourself, and stop caring about Tony for now.

Now, imagine that you've just sat down at your computer, ready to start working on your project. However, you forget to **pull** before working on your repo. You realize that the favorite song you wrote yesterday wasn't actually your favorite, so you type in a new one. You **commit** your change, and again, you forget to **pull** before you **push** to the online **repository**.

Do the above. You'll quickly see that this won't work, and you'll get an error looking like this:  
![gitTut6.png](https://bitbucket.org/repo/pkbBLj/images/2864716647-gitTut6.png)

The reason for this error is that the online **repository** is ahead with commits that aren't locally stored. What this means is that you *must* **pull** before you can actually **push** your changes. 

If you do a **pull** however, you will notice another problem, a **merge conflict**. 

### 2. What is a merge conflict and how do you fix it? ###
A **merge conflict** happens when two different **commits** try to change something at the same place in a file. In this case the **commit** created by Tony contains the original favorite song whereas your **commit** contains a new one. **Git** doesn't know automatically which line that should be used, so it doesn't accept the **pull** before the user, you, has decided which to use. If you've done the **pull** in SourceTree, you'll notice that SourceTree mentions that you have a **merge conflict** that needs to be fixed. The window will look something like this: 
![gitTut7.png](https://bitbucket.org/repo/pkbBLj/images/1872402956-gitTut7.png)
As you'll see in the right window, the file looks different now. It contains the information of both **commits**, as well as some less than signs, equal signs and larger than signs. Everything between the less than signs and the equal signs is from your **commit**, whereas everything between the equal signs and the larger than signs is from Tony's **commit**. It is now up to you to decide what is kept and later on **pushed** to the online **repo**. 

In SourceTree, there are a few preset options that can be used when handling **merge conflicts**. If you right-click the favorites file and go down to **Resolve Conflicts**, you'll see two options: **Resolve Using 'Mine'** and **Resolve Using 'Theirs'**. They do as they say, the first one keeps everything from your **commit** and removes everything else, whilst the second one keeps everything from Tony's **commit** and removes everything else. In this case however, we'll not be using any of those options. 

We want the new favorite song to stay, but we also want the favorite food to stay, but they're in separate **commits**. However, **merge conflicts** can also be solved in text editors. Double click on the favorites file and it'll open up in your text editor, looking like it did in the right window in SourceTree. Make sure that you keep the new favorite song, and the favorite food, and remove the old favorite song, the duplicate of the favorite TV-show, and the less than, equal and larger than signs, as well as the text on the same lines as those. After you're done, it should look something like this:

![gitTut8.png](https://bitbucket.org/repo/pkbBLj/images/1546611821-gitTut8.png)

Once you've done this, save the file and go back to SourceTree. You'll see that the file has now changed in the right window and looks alright, and you'll now have to stage and **commit** the **merged** file. When **committing** after a **merge conflict**, the **commit message** is automatically filled in, and there's no need to change any of that. Press **commit** and then **push** to your online **repo**. There shouldn't be any errors this time.

Most people find **merge conflicts** intimidating, but in reality, they are very easy to deal with, so just stay calm and make sure that you and the other collaborators are on the same page on what to keep in the project. 

### 3. How to make the commit history cleaner ###
One thing you'll notice when working with other people in **Git** is that when you **pull** before **pushing** and someone else has done some changes that aren't in the same file as your changes, the **commit** history will still contain a commit that mentions the **merge** of two or more **commits**. Moreover, your **commits** and the other person's **commits** will be separated in the tree looking image in the **commit** history. This doesn't look clean, and clutters up your history, making it harder to see how the project has evolved. However, there is a solution to this. When doing a **pull** before you **push** your **commits**, check the box marked **Rebase instead of merge**. As can be seen next to the text, make sure you do this *before* you **push**. This will make the **commit** history linear, and the **commits** mentioning **merges** won't appear. If you happen to stumble upon a **merge conflict** when doing this, you can solve it the same way as any other **merge conflict**. 

## **How to utilize issues and milestones in your project** ##
### 1. What are issues and milestones? ###
Now that you know your way around using **Git**, it's time to introduce some features with BitBucket that will help when creating your project, namely **issues** and **milestones**. These are the reason you checked the box marked with **Issue tracking** when creating your repository. 

**Issues** and **milestones** are a great way of describing what has to be done in the project. **Milestones** are larger goals that contain **issues**, and **issues** are smaller things that should be implemented. As an example, let's say you're creating a platformer game. Good **milestones** could then be the following:

* Create a working game engine
* The player
* Menus
* Enemies

These **milestones** then contain **issues** that describe in more detail what has to be done. For example, the first **milestone** might contain:

* Render the level and all objects in it onto the screen
* Make sure the game can handle user input
* Make sure objects interact with each other correctly, (ie. the player collides with walls)

**Issues** can also describe bugs that have to be fixed, like if the player gets stuck in a wall or something. To separate the various kinds of **issues**, they can be labeled as different kinds. They can also be given different priorities.

Once an **issue** has been solved, you can mark it as closed. This means that it's no longer an issue.

### 2. How to use issues and milestones ###
The main idea is that you can keep track on where you are in your project with these **issues** and **milestones**. As soon as you've completed something, you can close the **issue** related to it, and you'll get closer to completing a **milestone**. 

Practically, you can create **milestones** by going into the **Settings** tab in the left margin on BitBucket. Once there, find the **Milestones** tab, and create your **milestones**. **Issues** are created and maintained in the **Issues** tab in the left margin on BitBucket. Just create an **issue** and fill in the information you want to give it.

A cool thing about **issues** is that they can be closed in **commit messages**. Every **issue** is assigned a # followed by a number. When you close an **issue**, you can use one of the following words in conjunction with the number:

    
* close

* closes

* closed

* closing

* fix

* fixed

* fixes

* fixing

* resolve

* resolves

* resolved

* resolving

One example of a **commit message** could be "Fixed #23, player no longer gets stuck in walls".

## **How to use branches and pull requests** ##
### 1. What are branches? ###
Imagine the following scenario: You and your friend Tony are working on a game together. You don't really have anything to do at the moment, but you've come up with a great idea; laser sharks. However, you don't know if Tony would approve if you added it to the **repository**. You try to contact him to see what he thinks about it, but then you realize he's on vacation in Thailand for two weeks without any wifi. 

Now imagine this scenario: After a weekend of partying you come back to work on Monday, ready to get back to the project you're working on, but when you **pull** to your local **repo** you realize that Tony has been adding code everywhere in the project, and you no longer have a clue on what everything does. 

These scenarios are the reason for using **branches**. A **branch** is a separate part of your **repo** that can have files added to it, and those files can then be **committed** and **pushed**. The difference is that the **commits** will stay on that **branch**, and won't be accessible from any other place in the **repo**. This is perfect if you want to add laser sharks without potentially messing up the rest of the project, just add them to a new **branch**. The same goes for waking up to loads of new code, Tony could have put that in a **branch** and then let you have a look at the **branch** before accepting it. 

Every **repository** have a main **branch** called **master**, where all the main files are stored. **master** is the **branch** that's considered live version, this is where everything that makes the cut ends up.

### 2. How to use branches ###
To start using **branches**, you must first create one. In SourceTree, this is easily done by pressing the **Branch** button at the top of the window. Fill in a name for the new **branch** and press the **Create Branch** button. 

You can see your different **branches** in the **Branches** tab in the left margin in SourceTree. By double clicking on one of the **branches**, you'll switch to working on that **branch**. 

When you want to work on a **branch**, switch to it, and then add files, and **commit** and **push** as usual. The only difference is that when you push, you now choose the **branch** you're working on instead of **master**. By doing this, you keep adding to the project, without messing up the **master** version!

### 3. The what and how of pull requests ###
So what do you do when you're done with a **branch** and want the people you're collaborating with to accept it? This is where something called a **pull request** comes in. A **pull request** is a request for adding a **branch** to the **master** **branch**. It contains information about what has been done within the **branch**, and also a comment section for discussion. This is the nice and kind way of sharing things with your colleagues; instead of just **pushing** things in their faces, they get to have a say in what you've done before it's added to the main project. 

**Pull requests** are done on BitBucket. In the left margin, there's a tab called **Create pull request**. If you click on it, you'll be able to **merge** a **branch** with another, and add information and reviewers to it. The reviewers will be in charge of checking if it's good enough for the main project. Once the **pull request** is created, people can look at it, approve it, and either decline or **merge** it with the **branch** requested. It certainly is a great tool if you want to stay friends with your collaborators.

## **Final Words** ##
Congratulations! You've successfully completed this **Git** & BitBucket tutorial. I hope you've learned something useful, and that you'll be using version control in the future. :) 

Fredrik Omstedt 2016